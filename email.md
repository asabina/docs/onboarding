This checklist should guide you through the selection and configuration of your
mail client. It is important that you seek advise in case you run into any
issues with the instructions provided.

> All texts capitalized and formatted like `THIS` are meant to be replaced with
> the information provided to you. If you are missing any information, please
> reach out for assistance.

The following bits of information should be made available to you before you
start:
 - `COMPANY_BLOCK`
 - `DISCLAIMER_MOBILE`
 - `IMAP_SERVER_PORT`
 - `IMAP_SERVER_URI`
 - `SMTP_SERVER_PORT`
 - `SMTP_SERVER_URI`
 - `TEST_EMAIL`
 - `USERNAME`
 - `USER_EMAIL`
 - `USER_NAME`
 - `USER_PHONE`
 - `USER_ROLE`

# Choose a client
In order to be fully set up, it would be recommended to configure a client for
your computer and for your smartphone. There are different options available,
but since we are a FOSS company, we recommend FOSS solutions.

## Computers (Desktop or Laptop)
On desktop computers or laptops, we recommend Thunderbird as it works on Linux,
MacOS and Windows and is FOSS.

If you are using Outlook or Apple Mail, please note that the manner to
configure these may be different although generally pretty straightforward
(perhaps after a websearch).

> If you already use another mail client that you prefer, I would recommend
> that you stick to that client. Try to keep things easier for yourself and
> stick to the tools you know. You don't need to use Thunderbird because others
> do.

 - [ ] Choose and download client for your computer

## Smartphone
On smartphones, there are several options for mail.

> If you have a mail program that you prefer and suits would workflow best,
> keep using this mail application. We're not in the business of complicating
> our lives so let's keep things simple.

 - [ ] Choose and download client for your mobile device

### Android
A FOSS option for Android is k-9 Mail. Conversely, one may use any of the mail
clients that are often supplied with the phone by the OEM (the party that
manufactured the phone).

### Apple iOS
The default Mail application on Apple iOS will suffice in this scenario if you
already use it although we will not dive into the details on how to configure
this client specifically.

# Configure your clients

> Remember that your username `USERNAME` may be different from your e-mail
> address `USER_EMAIL`. Carefully observe that you provide the appropriate
> username as you configure your SMTP and IMAP settings.

## Background
SMTP is the Simple Mail Transfer Protocol and is the protocol used by your
client to actually send e-mails.

IMAP is the Internet Message Access Protocol and is used by clients access
messages from mail servers. POP3 is the Post Office Protocol which can be used
to retrieve messages and generally deletes those message from the server as
they are retrieved. IMAP is more flexible as it works well with more complex
folder structures and also simplifies the use of multiple devices. When using
POP3, one could retrieve and remove a message from a server, but that isn't
very useful when you later use another device and find yourself unable to
retrieve the message because it was previously downloaded unto a device. With
IMAP the messages remain on the server and are only removed when you explictly
delete them as such **IMAP is more flexible than POP3**.

## Security
From a security perspective we only use SSL or its successor TLS instead of
STARTTLS.

The reason is that STARTTLS connections start as unsecure connections that are
upgraded to secure connections upon request by the client shortly after
initiation. The problem is that sometimes the client and server do not succeed
in upgrading the connection to a secure connection and in that case continue
communications on a unsecured channel. This leaves one exposed to leak
username, password and other sensitive information that one is exchanging
through the mail client.

> Refrain from using clients that do not allow users to control the security
> settings for SMTP and IMAP use. Using regular IMAP instead IMAPS (IMAP
> secure) or SMTP instead of SMTPS (SMTP secure) could possibly leave one
> exposed to data leakage. Therefore confirm that the encryption or security
> type is selected to SSL or TLS and not just STARTTLS and if possible
> explicitly specify the ports to use such that they aren't simply inferred by
> the client.

 - [ ] Select and configure the SSL or TLS settings for your client

## Address Selection
The username `USERNAME` that you use to login to the mail servers
`IMAP_SERVER_URI` and `SMTP_SERVER_URI` may be different from the email address
`USER_EMAIL` that you use for communication. Exercise caution in ensuring that
your usernames are correct and configure your client to use your forward-facing
e-mail address in all communications.

Certain clients allow one to specify which address emails are sent from. In
many cases, one can select any of the alias addresses that have been configured
for a given mailbox. Select one of the `USER_EMAIL` options provided to you and
set this as your sending address and Reply-To address to ensure that users
direct replies to the address you intended to be used.

 - [ ] Configure the sender and Reply-To addresses

## HTML
E-mail was designed for textual communication. Using this medium to send HTML
documents possibly with assets such as images or scripts that may need to be
loaded from servers on the public internet is a practice that creates a lot of
room for abuse as these assets on the public internet could potentially execute
malignant code on your system.

> Don't send HTML emails and disable your client from displaying HTML e-mails
> if you receive them. Definitely disable JavaScript and image loading and
> rendering in your client. If you need a nicely designed brochure let the
> sender provide it to you as an attachment that you can possibly scan for
> malware before opening.

 - [ ] Configure the client to disable image loading or rendering
 - [ ] Configure the client to use plaintext instead of HTML

## Signature
Signatures are useful when they provide recipients the necessary information to
reach the sender. Therefore we contain a few items within our signatures and
expect all outgoing emails to stick to the standard we employ for outgoing
messages.

> We're professionals in the IT space! If one wants colorful signatures they
> have selected the wrong party to be associated with. We will not sacrifice
> prudent IT practices for aesthetics. There are ample routes to demonstrate
> creativity and individuality; signatures aren't it in this case. Those who
> understand the underlying technology, will generally disable insecure
> features and this may as a result break signatures that utilize images and or
> perhaps even HTML. Furthermore, signatures utilizing HTML and images are
> inaccessible to some audience and perhaps even noninclusive if you have to
> think about readers that utilize accessibility aids to make sense of messages
> targeted at them. Plaintext signatures are practical for braille devices and
> screenreaders and legible in every mail client that you will find out there.

 - name: although already clear from the e-mail, a printed version of an
   email may not always clearly present the sender of a given message.
   Providing this information in the signature ensures that the proper context
   is available to the reader regardless of the medium in which presented.
 - phonenumber: only document a phonenumber if this phonenumber is provided to
   you by the company. Your work-life and private-life are separate domains for
   good reasons. Providing a personal number in this slot makes it harder when
   an incoming phonecall is professional or private and help reduce the count
   of awkward or unprofessional episodes.
 - email: Eventhough your e-mail should be presented in the header of the
   e-mail there is no guarantee that this is true for every medium on which
   this message will be presented. Presenting this information in the footer
   therefore increases the changes that the right email address will be legible
   to a reader of a message in a thread or print form.
 - company info: information about the company such as the company's address,
   directors, legal name, tax numbers and register identifiers provide
   recipients with more context about the entity they are dealing with. This
   string should be provided to you to ensure all outgoing communication meets
   the same standard with regards to formatting.

The following snippet provides an example of how your signature is to be
formatted:

```
-- 
USER_NAME
USER_ROLE
USER_PHONE
USER_EMAIL

COMPANY_BLOCK

DISCLAIMER_MOBILE
```

Display a `DISCLAIMER_MOBILE` only if you are configuring a signature for a
mail client on a mobile device. The following string could be substituted for
`DISCLAIMER_MOBILE`:

```
Sent from a mobile device.
```

 - [ ] Configure the signature or footer for outgoing e-mails

# Test
In order to complete your email configuration process sent an email to the test
address `TEST_EMAIL` provided at which we automatically test that your
configurations are set appropriately.

> Do not forget to test a client every time you set it up. Furthermore it is a
> good practice to send test emails to the provided test address `TEST_EMAIL`
> on a regular basis in order to retest the correctness of your client setup in
> relation to company policy.

 - [ ] Send test email to `TEST_EMAIL` once you're ready to get stuff done
