# OSX Onboarding

- Bash or zsh (as the terminal of choice)
- brew (as package manager)

## Git

For simplicity's sake, one should enable shell completions which may be done
through the following brew install commands for bash and zsh respectively:

```
# For Bash
brew install git bash-completion

# For zsh
brew install git zsh-completion
```

Complete the instructions provided in the terminal and then restart your
terminal in order to have shell completion activated.

## Xcode Development Tools

Ensure that the Xcode Development Tools are installed prior to continuing.

```
xcode-select --install
```

## iOS Development

We use Fastlane, to manage certificates and provisioning profiles for iOS
development. Note that the Fastlane tooling will access git repositories and
that this requires your setup to add your SSH key to your ssh-agent by running

```
ssh-agent ~/.ssh/PATH_TO_PRIVATE_KEY
```

in order to ensure that Fastlane commands can access the necessary git
repositories even when there is no way to produce a passphrase prompt.

> In order to ensure that your key is loaded, run the ssh-agent command after a
> restart or wake-up event.

## Fastlane

Use `tree -C -L 3` inside of the fastlane assets repository in order to examine
its assets and run the following command to observe the installed codesigning
identities for comparison:

```
security find-identity -v -p codesigning
```

## iPhone/iPad (iOS Device) Setup

The _Admin_ and _Account Holder_ roles for the Asabina GmbH Apple Developer
account is currently assigned to david@asabina.de, but a Team Account has been
created as apple-dev@asabina.de with _App Manager_ and _Customer Support_
roles.

> Automation tooling is expected to utilize the apple-dev@asabina.de
> credentials and for simplicity's sake all certificates and profiles managed
> through Fastlane should utilize the apple-dev@asabina.de account.

## 2FA

Consult the Apple Developer [Two-factor Authentication][apple-dev-2fa]
documentation in order to learn how to configure 2FA for your setup.

[apple-dev-2fa]: https://developer.apple.com/support/authentication/
